# Crosspost

An image crossposting tool that supports...

 * An SFTP Dir + Gemini Blog
 * Instagram
 * Pixelfed / Mastodon
 * Cohost
 * Discord
 * *Unsplash* (WIP)
 * *Tumblr* (WIP)
